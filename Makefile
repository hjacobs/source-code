.PHONY: clean test appjs docker push mock

IMAGE            ?= hjacobs/source-code
GITDIFFHASH       = $(shell git diff | md5sum | cut -c 1-4)
VERSION          ?= $(shell git describe --tags --always --dirty=-dirty-$(GITDIFFHASH))
VERSIONPY         = $(shell echo $(VERSION) | cut -d- -f 1)
TAG              ?= $(VERSION)
TTYFLAGS          = $(shell test -t 0 && echo "-it")
OSNAME := $(shell uname | perl -ne 'print lc($$_)')

default: docker

.PHONY: poetry
poetry:
	poetry install

.PHONY: test
test: poetry lint test.unit test.e2e

.PHONY: lint
lint:
	poetry run pre-commit run --all-files

.PHONY: test.unit
test.unit:
	poetry run coverage run --source=source_code -m py.test tests/unit
	poetry run coverage report

docker:
	docker build --build-arg "VERSION=$(VERSION)" -t "$(IMAGE):$(TAG)" .
	@echo 'Docker image $(IMAGE):$(TAG) can now be used.'

push: docker
	docker push "$(IMAGE):$(TAG)"
	docker tag "$(IMAGE):$(TAG)" "$(IMAGE):latest"
	docker push "$(IMAGE):latest"

mock:
	docker run $(TTYFLAGS) -p 8080:8080 "$(IMAGE):$(TAG)" --mock

.PHONY: run
run:
	poetry run python3 -m source_code

.PHONY: version
version:
	# poetry only accepts a narrow version format
	sed -i "s/^version = .*/version = \"${VERSIONPY}\"/" pyproject.toml
	sed -i "s/^__version__ = .*/__version__ = \"${VERSION}\"/" source_code/__init__.py
	sed -i "s/v=[0-9A-Za-z._-]*/v=${VERSION}/g" source_code/templates/base.html
	sed -i "s,hjacobs/source-code:.*,hjacobs/source-code:${VERSION}," deploy/deployment.yaml
