import logging
from pathlib import Path

import aiohttp_jinja2
import jinja2
from aiohttp import web

from source_code import __version__

logger = logging.getLogger(__name__)

HEALTH_PATH = "/health"

CONFIG = "config"

routes = web.RouteTableDef()


@routes.get("/")
@aiohttp_jinja2.template("index.html")
async def get_index(request):
    # config = request.app[CONFIG]

    return {}


@routes.get(HEALTH_PATH)
async def get_health(request):
    return web.Response(text="OK")


def get_app(config):
    templates_paths = [str(Path(__file__).parent / "templates")]
    if config.templates_path:
        # prepend the custom template path so custom templates will overwrite any default ones
        templates_paths.insert(0, config.templates_path)

    static_assets_path = Path(__file__).parent / "templates" / "assets"
    if config.static_assets_path:
        # overwrite assets path
        static_assets_path = Path(config.static_assets_path)

    app = web.Application()
    aiohttp_jinja2.setup(
        app,
        loader=jinja2.FileSystemLoader(templates_paths),
        trim_blocks=True,
        lstrip_blocks=True,
    )
    env = aiohttp_jinja2.get_env(app)
    env.globals["version"] = __version__

    app.add_routes(routes)
    app.router.add_static("/assets", static_assets_path)

    app[CONFIG] = config

    return app
