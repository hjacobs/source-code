import argparse
import asyncio
import datetime
import logging
import random
import subprocess
from pathlib import Path
from urllib.parse import urlparse

import aiohttp.web
import httpx

from .web import CONFIG
from .web import get_app
from source_code import __version__


logger = logging.getLogger(__name__)

DEFAULT_TIMEOUT = 30
DEFAULT_JITTER = 10

HOMEPAGE = "https://codeberg.org/hjacobs/source-code"


def comma_separated_values(value):
    return list(filter(None, value.split(",")))


def parse_args(argv=None):
    parser = argparse.ArgumentParser(description=f"Source Code v{__version__}")
    parser.add_argument(
        "--port",
        type=int,
        default=8080,
        help="TCP port to start webserver on (default: 8080)",
    )
    parser.add_argument(
        "--version", action="version", version=f"source-code {__version__}"
    )
    parser.add_argument(
        "--debug", action="store_true", help="Run in debugging mode (log more)"
    )
    parser.add_argument(
        "--sites",
        type=comma_separated_values,
        default=[],
        help="Sites to monitor (comma separated list of URLs)",
    )
    parser.add_argument(
        "--data-path",
        default=".",
        help="Path to directory to store monitoring data",
    )
    # customization options
    parser.add_argument(
        "--templates-path", help="Path to directory with custom HTML/Jinja2 templates"
    )
    parser.add_argument(
        "--static-assets-path",
        help="Path to custom JS/CSS assets (will be mounted as /assets HTTP path)",
    )
    args = parser.parse_args(argv)
    return args


async def background_process(app):
    config = app[CONFIG]
    repositories_path = Path(config.data_path) / "repositories.tsv"
    headers = {"User-Agent": f"source-code/{__version__} ({HOMEPAGE})"}
    while True:
        try:
            for site in config.sites:
                parsed_site_url = urlparse(site)
                logger.debug(f"Probing {site} ..")
                try:
                    async with httpx.AsyncClient() as client:
                        response = await client.get(
                            site, timeout=DEFAULT_TIMEOUT, headers=headers
                        )
                        now = datetime.datetime.utcnow()
                        data = response.json()
                        for org in data:
                            logger.info(f"Processing org {org['username']}..")
                            response = await client.get(
                                site + f"/{org['username']}/repos",
                                timeout=DEFAULT_TIMEOUT,
                                headers=headers,
                            )
                            repos = response.json()
                            for repo in repos:
                                logger.info(f"Processing {repo['html_url']}..")
                                with repositories_path.open("a") as fd:
                                    fd.write(
                                        "\t".join(
                                            map(
                                                str,
                                                [
                                                    now.isoformat(),
                                                    repo["html_url"],
                                                    repo["description"],
                                                    repo["fork"],
                                                    repo["archived"],
                                                    repo["stars_count"],
                                                ],
                                            )
                                        )
                                        + "\n"
                                    )

                                path = (
                                    Path(config.data_path)
                                    / parsed_site_url.netloc
                                    / org["username"]
                                )
                                path.mkdir(parents=True, exist_ok=True)

                                repo_dir = path / f"{repo['name']}.git"

                                if repo_dir.exists():

                                    cmd = "/usr/bin/git"
                                    args = ["remote", "update"]
                                    proc = await asyncio.create_subprocess_exec(
                                        cmd,
                                        *args,
                                        stdout=asyncio.subprocess.PIPE,
                                        stderr=asyncio.subprocess.PIPE,
                                        cwd=str(repo_dir),
                                    )

                                    stdout, stderr = await proc.communicate()
                                    print(stdout, stderr)
                                else:

                                    cmd = "/usr/bin/git"
                                    args = [
                                        "clone",
                                        "--mirror",
                                        repo["html_url"],
                                        repo_dir,
                                    ]
                                    proc = await asyncio.create_subprocess_exec(
                                        cmd,
                                        *args,
                                        stdout=asyncio.subprocess.PIPE,
                                        stderr=asyncio.subprocess.PIPE,
                                        cwd=str(path),
                                    )

                                    stdout, stderr = await proc.communicate()
                                    print(stdout, stderr)

                except Exception as e:
                    logger.exception(f"Failed to probe {site}: {e}")
        except Exception as e:
            logger.exception(f"Failed to process: {e}")
        sleep_time = max(0.1, 300) + random.gauss(0, DEFAULT_JITTER)
        await asyncio.sleep(sleep_time)


async def start_background_tasks(app):
    app["background_task"] = asyncio.create_task(background_process(app))


async def cleanup_background_tasks(app):
    app["background_task"].cancel()
    await app["background_task"]


def main(argv=None):
    args = parse_args(argv)

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)

    config_str = ", ".join(f"{k}={v}" for k, v in sorted(vars(args).items()))
    logger.info(f"Source Code v{__version__} started with {config_str}")

    app = get_app(args)
    app.on_startup.append(start_background_tasks)
    app.on_cleanup.append(cleanup_background_tasks)
    aiohttp.web.run_app(app, port=args.port, handle_signals=False)
